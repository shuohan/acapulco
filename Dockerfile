# FROM tensorflow/tensorflow:1.7.0-py3
FROM tensorflow/tensorflow:1.7.0-gpu-py3

ARG shan_n4=0.2.1
ARG shan_mni=0.1.2
ARG simple_pipeline=0.2.0
ARG kki_parc_model=4bf46a4b05228bf3b884cd11bf7176be
ARG kki_bbox_model=00fd12f857b042fe17d11bb0361a1fb9
ARG tmc_parc_model=691a423d668decf8092b701bb921ec6f
ARG tmc_bbox_model=8b6d4a0aaa5a8dbb711fc0cc98ec0cf4
ARG keras=2.1.5
ARG keras_contrib=b3dc815
ARG acapulco=0.3.0
ARG improc3d=0.4.1
ARG label_image_cleanup=0.2.0
ARG seg_utils=0.0.1
ARG segviz=2.3.0
ARG nibabel=2.5.1
ARG dataset=b1996b6

LABEL robex=1.2 ants=2.3.1 mni=icbm152_2009c_1mm fsl=6.0.0 keras=${keras} \
    keras_contrib=${keras_contrib} shan_n4=${shan_n4} shan_mni=${shan_mni} \
    simple_pipeline=${simple_pipeline} acapulco=${acapulco} \
    improc3d=${improc3d} label_image_cleanup=${label_image_cleanup} \
    tmc_parc_model=${tmc_parc_model} tmc_bbox_model=${tmc_bbox_model} \
    kki_parc_model=${kki_parc_model} kki_bbox_model=${kki_bbox_model} \
    seg_utils=${seg_utils} segviz=${segviz} dataset=${dataset}

WORKDIR /tmp
RUN apt-get update && apt-get install -y unzip libopenblas-dev vim git
RUN pip3 install --no-cache-dir keras==${keras} scikit-learn \
        && git clone -b ${shan_n4} https://gitlab.com/shan-processing/n4.git /opt/n4 \
        && git clone -b ${shan_mni} https://gitlab.com/shan-processing/mni.git /opt/mni \
        && git clone -b ${simple_pipeline} https://gitlab.com/shan-processing/simple-pipeline.git /opt/simple-pipeline \
        && pip3 install git+https://gitlab.com/shan-deep-networks/dataset.git@${dataset} \
        && pip3 install git+https://gitlab.com/shan-utils/segviz.git@${segviz} \
        && pip3 install git+https://www.github.com/keras-team/keras-contrib.git@${keras_contrib} \
        && pip3 install git+https://gitlab.com/shuohan/acapulco.git@${acapulco} \
        && pip3 install git+https://gitlab.com/shan-utils/improc3d.git@${improc3d} \
        && pip3 install git+https://gitlab.com/shan-utils/label-image-cleanup.git@${label_image_cleanup} \
        && pip install git+https://gitlab.com/shan-utils/seg-utils.git@${seg_utils} \
        && mkdir -p /opt/ants/bin && mkdir -p /opt/fsl/bin

COPY --from=ants:2.3.1-ubuntu16.04 /opt/ants/bin/antsRegistration \
    /opt/ants/bin/antsApplyTransforms /opt/ants/bin/ResampleImageBySpacing \
    /opt/ants/bin/ConvertTransformFile /opt/ants/bin/antsAffineInitializer \
    /opt/ants/bin/N4BiasFieldCorrection /opt/ants/bin/
COPY --from=fsl:6.0.0 /opt/fsl/bin/avscale /opt/fsl/bin/
COPY models /opt/models
COPY ROBEX /opt/ROBEX
COPY tmc_labels.json kki_labels.json /opt/
RUN [ `md5sum /opt/models/tmc_parc_model.h5 | sed "s/ .*$//"` = ${tmc_parc_model} ] \
        && [ `md5sum /opt/models/tmc_bbox_model.h5 | sed "s/ .*$//"` = ${tmc_bbox_model} ] \
        && [ `md5sum /opt/models/kki_parc_model.h5 | sed "s/ .*$//"` = ${kki_parc_model} ] \
        && [ `md5sum /opt/models/kki_bbox_model.h5 | sed "s/ .*$//"` = ${kki_bbox_model} ]

RUN rm -rf /tmp/*

ENV PATH=/opt/ants/bin:/opt/fsl/bin:/opt/ROBEX:/opt/mni/scripts:/opt/n4/scripts:/opt/simple-pipeline:/opt/niftyreg/bin:$PATH
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/niftyreg/lib
ENV MNIDIR=/opt/mni/template

WORKDIR /tmp
RUN git clone git://git.code.sf.net/p/niftyreg/git niftyreg-git \
        && apt-get install -y wget \
        && wget https://github.com/Kitware/CMake/releases/download/v3.21.1/cmake-3.21.1-linux-x86_64.sh \
        && mkdir -p /opt/cmake3 \
        && sh ./cmake-3.21.1-linux-x86_64.sh --prefix=/opt/cmake3 --skip-license \
        && mkdir -p /tmp/niftyreg-build /opt/niftyreg \
        && /opt/cmake3/bin/cmake -DCMAKE_INSTALL_PREFIX=/opt/niftyreg -DCMAKE_BUILD_TYPE=Release /tmp/niftyreg-git \
        && make -j 4 && make install && rm -rf /tmp/*

ENTRYPOINT ["/usr/local/bin/acapulco.sh"]
CMD ["-h"]
