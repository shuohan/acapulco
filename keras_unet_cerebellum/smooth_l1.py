# -*- coding: utf-8 -*-

from functools import partial
from keras import backend as K
import tensorflow as tf


def calc_smooth_l1_loss(y_true, y_pred):
    x = K.abs(y_true - y_pred)
    x = tf.where(tf.less(x, 1.0), 0.5 * x ** 2, x - 0.5)
    return  K.mean(x)
