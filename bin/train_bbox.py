#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from argparse import ArgumentDefaultsHelpFormatter

desc = ''
parser = argparse.ArgumentParser(description=desc,
                                 formatter_class=ArgumentDefaultsHelpFormatter)

help = ''
parser.add_argument('image_dir', help=help)

help = ('The indices of the validation data among the images in ${image_dir}. '
        'Other images will be used in training.')
parser.add_argument('-v', '--validation-indices', nargs='+', type=int,
                    default=[], required=False, help=help)

help = ('The the prefix of the paths to the trained models and losses that are '
        'stored periodically. The models are saved to '
        '${output_prefix}_model_${epoch_ind}.h5 and the losses are stored in a '
        'single .csv file ${output_prefix}_log.csv.')
parser.add_argument('-o', '--output-prefix', required=True, help=help)

parser.add_argument('-b', '--batch-size', type=int, default=1, required=False,
                    help='Number of samples per batch')
parser.add_argument('-e', '--num-epoches', type=int, default=200,
                    required=False, help='Number of epoches')
parser.add_argument('-a', '--augmentation', nargs='+', 
                    default=['none', 'flipping', 'rotation', 'deformation'],
                    required=False, help='Types of data augmentation',
                    choices=['none', 'flipping', 'rotation', 'deformation',
                             'translation', 'scaling'])

help = ('The max scaling factors along x, y, z axes of the random scaling. '
        'The factors are randomly sampled from a uniform distribution '
        '[1, ${max_scaling_factor}] and skrinking and enlarging has '
        'probability 0.5.')
parser.add_argument('-sf', '--max-scaling-factor', type=float, default=1.6,
                    required=False, help=help)

help = ('The max angles along x, y, z axes of the random rotation. The angles '
        'are randomly sampled from a uniform distribution '
        '[-${max_rotation_rangle}, ${max_rotation_angle}]')
parser.add_argument('-r', '--max-rotation-angle', type=int, default=15,
                    required=False, help=help)

help = ('The max absolute translation along x, y, z axes of the random '
        'translation. The translations are randonly sampled from a uniform '
        'distribution [-${max_translation}, ${max_translation}]')
parser.add_argument('-t', '--max-translation', type=int, default=30,
                    required=False, help=help)
parser.add_argument('-d', '--num-encoders', type=int, default=6, required=False,
                     help='The number of encoders.')
parser.add_argument('-n', '--normalization', choices=['batch', 'instance'],
                    default='instance', required=False,
                    help='The normalization layer used in the U-Net')
parser.add_argument('-y', '--load-on-the-fly', required=False,
                    action='store_true', default=False,
                    help='Load data on the fly')

help = 'The number of the features of the output of the input block.'
parser.add_argument('-f', '--num-input-block-features', type=int, default=32,
                    required=False, help=help)

help = ('The label pairs to swap after flipping augmentation so the right/left '
        'labels are still at right/left after flipping the images. Each pair '
        'is separated by a white space; the two labels of a pair is separated '
        'by a comma (without any whitespace!).')
parser.add_argument('-l', '--label-pairs', default=None, required=False,
                    nargs='+', help=help)

parser.add_argument('-i', '--input-model', required=False, default='',
                    help='Input model for continuing training')
parser.add_argument('-w', '--kernel-initialization', required=False,
                    default='glorot_normal', help='Weight initialization')
parser.add_argument('-k', '--leaky-relu-alpha', required=False, type=float,
                    default=0.1, help='LeakyReLU negative slope')
parser.add_argument('-p', '--save-period', required=False, default=10,
                    type=int, help='The model saving period.')
parser.add_argument('-s', '--dropout-rate', required=False, default=0.2,
                    type=float, help='Spatial dropout rate in decoders')

parser.add_argument('-in', '--not-include-none', required=False, default=False,
                    action='store_true',
                    help='Do not include original images in augmentation')
parser.add_argument('-if', '--not-include-flipped', required=False,
                    default=False, action='store_true',
                    help='Do not include flipped images in augmentation')

args = parser.parse_args()

from glob import glob
import numpy as np
import os

import keras.backend as K
from keras.callbacks import ModelCheckpoint, CSVLogger
from keras.optimizers import Adam
from network_utils import TrainingDataFactory as DF
from network_utils import Dataset3dFactory as DSF

from keras_unet_cerebellum.networks import ActivationFactory
from keras_unet_cerebellum.networks import NormalizationFactory
from keras_unet_cerebellum.networks import InputFactory
from keras_unet_cerebellum.networks.outputs import RegressionOutputFactory
from keras_unet_cerebellum.networks.outputs import RegressAvgOutputFactory
from keras_unet_cerebellum.networks.bounding_box import BoundingBoxFactory
from keras_unet_cerebellum.networks.bounding_box import BboxFactoryDecorator
from keras_unet_cerebellum.networks import ResidueEncoderFactory
from keras_unet_cerebellum import Configuration, calc_smooth_l1_loss
from keras_unet_cerebellum import DataGeneratorFactory


image_paths = sorted(glob(os.path.join(args.image_dir, '*_image.*')))
mask_paths = sorted(glob(os.path.join(args.image_dir, '*_mask.*')))

if args.label_pairs is None:
    args.label_pairs = [[33, 36], [43, 46], [53, 56], [63, 66], [73, 76],
                        [74, 77], [75, 78], [83, 86], [84, 87], [93, 96],
                        [103, 106]]
else:
    args.label_pairs = [list(map(lp.split(','), int))
                        for lp in args.label_pairs]

config = Configuration()
data_factory = DF(dim=config.channel_axis, label_pairs=args.label_pairs,
                  max_angle=args.max_rotation_angle,
                  max_trans=args.max_translation,
                  max_scale=args.max_scaling_factor,
                  get_data_on_the_fly=args.load_on_the_fly,
                  types=args.augmentation)
data_factory = BboxFactoryDecorator(data_factory)
t_dataset, v_dataset = DSF.create(data_factory, args.validation_indices,
                                  image_paths, mask_paths,
                                  include_none=(not args.not_include_none),
                                  include_flipped=(not args.not_include_flipped))

print('-' * 80)
print('TRAINING')
for d in t_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(t_dataset))

print('-' * 80)
print('VALIDATION')
for d in v_dataset.data:
    print(*[dd.filepath for dd in d])
print('Number:', len(v_dataset))
print('-' * 80)

data_generator_factory = DataGeneratorFactory(batch_size=args.batch_size)
training_generator = data_generator_factory.create(t_dataset)
validation_generator = data_generator_factory.create(v_dataset)

if not os.path.isfile(args.input_model):
    activation_kwargs=dict(alpha=args.leaky_relu_alpha)
    input_factory = InputFactory(kernel_initialization=args.kernel_initialization,
                                 normalization=args.normalization,
                                 activation_kwargs=activation_kwargs)
    encoder_factory = ResidueEncoderFactory(kernel_initialization=args.kernel_initialization,
                                            dropout_rate=args.dropout_rate,
                                            normalization=args.normalization,
                                            activation_kwargs=activation_kwargs)
    # output_factory = RegressionOutputFactory(activation_kwargs=activation_kwargs)
    output_factory = RegressAvgOutputFactory(activation_kwargs=activation_kwargs)
    bbox_factory = BoundingBoxFactory(input_factory, encoder_factory,
                                      output_factory)
    input_shape = t_dataset[0][0].shape
    num_values = t_dataset[0][1].shape[0]
    bbox_net = bbox_factory.create(input_shape, args.num_input_block_features,
                                   args.num_encoders, num_values=num_values)
    learning_rate = 1e-3
    bbox_net.compile(optimizer=Adam(lr=learning_rate), loss=calc_smooth_l1_loss)

else:
    from keras.models import load_model
    from keras_contrib.layers import InstanceNormalization
    custom_objects={'calc_smooth_l1_loss': calc_smooth_l1_loss,
                    'InstanceNormalization': InstanceNormalization}
    bbox_net = load_model(args.input_model, custom_objects=custom_objects)
    args.output_prefix = args.output_prefix + '_continue'

bbox_net.summary()
for layer in bbox_net.layers:
    if 'model' in layer.name:
        print(layer.name)
        layer.summary()

for layer in bbox_net.layers:
    if 'model' in layer.name:
        for ll in layer.layers:
            if 'spatial_dropout' in ll.name:
                print('dropout', ll.rate)
            if 'leaky_re_lu' in ll.name:
                print('leaky_re_lu', ll.alpha)
            if 'conv3d' in ll.name:
                print('conv3d', ll.kernel_initializer.distribution,
                      ll.kernel_initializer.scale, ll.kernel_initializer.mode)

model_path_prefix = args.output_prefix + '_model_{epoch:03d}.h5'
log_path = args.output_prefix + '_log.csv'

if len(args.validation_indices) > 0:
    bbox_net.fit_generator(generator=training_generator,
                           epochs=args.num_epoches,
                           steps_per_epoch=len(t_dataset)//args.batch_size,
                           validation_data=validation_generator,
                           validation_steps=len(v_dataset)//args.batch_size,
                           callbacks=[ModelCheckpoint(model_path_prefix,
                                                      save_best_only=False,
                                                      period=args.save_period),
                                      CSVLogger(log_path, append=True)])
else:
    bbox_net.fit_generator(generator=training_generator,
                           epochs=args.num_epoches,
                           steps_per_epoch=len(t_dataset)//args.batch_size,
                           callbacks=[ModelCheckpoint(model_path_prefix,
                                                      save_best_only=False,
                                                      period=args.save_period),
                                      CSVLogger(log_path, append=True)])

final_model_path = args.output_prefix + '_model_final.h5'
bbox_net.save(final_model_path)
