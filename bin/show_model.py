#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser(description='Show keras model')
parser.add_argument('input_model', help='Path to the model to show')
parser.add_argument('output_dir', help='Save .png to this folder')
parser.add_argument('-s', '--show-shapes', help='Show layer shapes',
                    action='store_true', default=False)
args = parser.parse_args()

import os
from keras.utils import plot_model
from keras.models import load_model
from keras_contrib.layers import InstanceNormalization

from keras_unet_cerebellum import calc_aver_dice_loss, calc_smooth_l1_loss


co = {'InstanceNormalization': InstanceNormalization,
      'calc_aver_dice_loss': calc_aver_dice_loss,
      'calc_smooth_l1_loss': calc_smooth_l1_loss}
model = load_model(args.input_model, custom_objects=co)

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

basename = os.path.basename(args.input_model).replace('.h5', '.png')
model_path = os.path.join(args.output_dir, basename)
print(model_path)
plot_model(model, to_file=model_path, show_shapes=args.show_shapes)

for layer in model.layers:
    if 'model' in layer.name:
        layer_basename = basename.replace('.png', '_'+layer.name+'.png')
        layer_path = os.path.join(args.output_dir, layer_basename)
        print(layer_path)
        plot_model(layer, to_file=layer_path, show_shapes=args.show_shapes)
