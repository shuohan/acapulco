## ACAPULCO: Automatic Cerebellum Anatomical Parcellation Using U-Net with Locally Constrained Optimization

#### | [Paper][paper] | [Docker] | [Singularity CPU][sif_cpu] | [Singularity GPU][sif_gpu] |

### Note

Previous versions have some randomness in the MNI registration step which causing low replicability; i.e., repetitive runs do not have the same result (see this [article](https://pubmed.ncbi.nlm.nih.gov/33421018/)). This is fixed in current v0.3.0 by using NiftyReg (see below).

The links to download ACAPULCO are right below the title heading. The CPU version is to apply pre-trained models, the GPU version is to train the models.

### Algorithm

The following processing steps are performed:

1. **N4 bias field correction**. [ROBEX](https://www.nitrc.org/projects/robex) is used to estimate a brain mask. This mask is then smoothed to generate a brain weight image. N4 from [ANTs][ANTs] is used to perform the bias field correction with the weight image calculated above. For the use of a weighted image in N4, check the [document online](https://itk.org/Doxygen/html/classitk_1_1N4BiasFieldCorrectionImageFilter.html) and the N4 [paper](https://pubmed.ncbi.nlm.nih.gov/20378467/). A wrapper function is hosted at this [link](https://gitlab.com/shan-processing/n4).

2. **MNI registration**. The images are rigidly registered to the [ICBM 2009c](http://www.bic.mni.mcgill.ca/ServicesAtlases/ICBM152NLin2009) nonlinear symmetric template. **Update v0.3.0: We use [NiftyReg](http://cmictig.cs.ucl.ac.uk/wiki/index.php/NiftyReg_documentation) `reg_aladin` to do the MNI registration. See [link](https://gitlab.com/shan-processing/simple-pipeline/-/blob/master/preprocessing_pipeline.sh) for the specific command that we use.**

3. **Cerebellum parcellation**. The cerebellum of an MNI-registered MPRAGE image is parcellated using the method described in our [paper][paper]. In summary, we first predict a bounding box around the cerebellum, **Update v0.3.0: We provide an option to use an averaged cerebellum mask in MNI space to calculate the bounding box**. We then crop out the cerebellum with this bounding box, and use a modified U-Net to parcellate it into subregions. Removing the neck should improve the results, such as using `robustfov` from [fsl](https://fsl.fmrib.ox.ac.uk/fslcourse/lectures/practicals/intro2/index.html), but it is not done in this algorithm. The post processing is based on largest connected components which is also described in our [paper][paper].

4. **Transform back to the original space**. Since parcellation is performed in MNI space, we additionally transform the post-processed parcellation into the original image space. **Note: We use nearest-neighbor interpolation to transform the parcellation back to its original space. It is highly recommanded to use the image in MNI space (see below for the path to this file) for measurements.**

5. **Volume of each region**. The volume in mm^3 of each region is also calculated.

### Installation

To use Docker,

```bash
# CPU version
docker pull registry.gitlab.com/shuohan/acapulco:0.3.0
```

or

```bash
# GPU version
docker pull registry.gitlab.com/shuohan/acapulco:0.3.0-gpu
```

See [this link](https://docs.docker.com/engine/install/) to install Docker Engine and [this link](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) for NVIDIA Docker to run the GPU version.

Note that we used Singularity v3.7 to create the Singularity images from the Docker images. See [this link](https://sylabs.io/guides/3.7/user-guide/singularity_and_docker.html#locally-available-images-stored-archives). Earlier versions of Singularity might not work with the Singularity images that we provided here. You might want to create them yourself from the Docker images or update Singularity. The links to the [CPU](sif_cpu) and [GPU](sif_gpu) Singularity images.

### Usage

```bash

# adult parcellation protocol
docker run -v $PWD:$PWD -w $PWD -t --user $(id -u):$(id -g) --rm acapulco:0.3.0 \
    -i sample_image.nii.gz -o sample_output_directory -m tmc

# adult parcellation protocol, use a cerebellum mask in MNI space
docker run -v $PWD:$PWD -w $PWD -t --user $(id -u):$(id -g) --rm acapulco:0.3.0 \
    -i sample_image.nii.gz -o sample_output_directory -m tmc -u

# pediatric parcellation protocol
docker run -v $PWD:$PWD -w $PWD -t --user $(id -u):$(id -g) --rm acapulco:0.3.0 \
    -i sample_image.nii.gz -o sample_output_directory -m kki

# use GPU
docker run --gpu device=0 -v $PWD:$PWD -w $PWD -t --user $(id -u):$(id -g) --rm acapulco:0.3.0-gpu \
    -i sample_image.nii.gz -o sample_output_directory -m tmc

# train with new dataset
mkdir results
docker run --gpus device=1 -w $PWD -v $PWD:$PWD --user $(id -u):$(id -g) --rm -it \
    --entrypoint train_parc.py acapulco:0.3.0-gpu train_parc.py training_data -o results/prefix

# print help
docker run --rm -it acapulco:0.3.0

# use singularity
singularity run -B $PWD:$PWD acapulco_030.sif -i 2763_mprage_deface.nii -o kki -m kki
singularity exec --nv -B $PWD:$PWD acapulco_030_gpu.sif train_parc.py training_data -o results/prefix

```

**Explanation**:

* `-v`: In the `docker run` command, the option `-v HOST:GUEST` is to mount the host directory into the container. In this example, the current folder, which is stored in the `$PWD` environment variable in Linux, is mounted to the same folder name under the Docker container. If your images in different folder, please mount that folder accordingly, such as `-v /path/to/folder:/path/to/folder`.
* `-w`: In the `docker run` command, `-w FOLDER_PATH` is working directory. It means that when you run this container, the start path is set to this folder path. If `-w` is set, you could use relative path to the image when running the processing. For example, if your image is `/path/to/folder/image.nii.gz`, you could set `-w /path/to/folder`, then you could use `image.nii.gz` as the input filename.
* `-t`: In the `docker run` command, `-t` gives you a tty, so the processing message can be printed to the screen.
* `--user`: In the `docker run` command, `--user USER_ID:GROUP_ID` sets the permission of the processing results to your user account and group instead of root, so you could manage them properly after the processing finished.
* `--rm`: In the `docker run` command, `--rm` removes the stopped container (the Docker image is still there. Every time a Docker image is run, a container is created) after the processing.
* `-i`, `-o`: These are ACAPULCO options. They are the input filename and the output folder name, respectively. Note that they are relative to the working directory (set by `-w`) in the Docker container.

**NOTE**:

* (**Important**) When using the Singularity images, be sure to set "channel_first" in `~/.keras/keras.json` in your home directory. This is because Singularity will automatically mount your home directory into the container, and keras settings in your home directory can cause problems. An example `keras.json` of mine is
    ```
    {
        "floatx": "float32",
        "backend": "tensorflow",
        "epsilon": 1e-07,
        "image_data_format": "channels_first"
    }

    ```
* It might not work if the image path contains extra `.nii` before the image file extension.


## Generate an HTML report

To generate an HTML file of segmentation overlays of multiple subjects, we have a tool at [here](https://github.com/shuohan/segviz). See its [documentation](https://shan-utils.gitlab.io/segviz/) for more details.

## Outputs

Suppose the output_directory is `output_dir`, the input image is `image.nii.gz`
* The MNI aligned image is `output_dir/mni/image_n4_mni.nii.gz`
* The parcellation is `output_dir/parc/image_n4_mni_seg.nii.gz`
* The post-processed parcellation is `output_dir/parc/image_n4_mni_seg_post.nii.gz`
* The parcellation in the original space is `output_dir/image_n4_mni_seg_post_inverse.nii.gz`
* The volumes of each region (calculated from the image in MNI space) are `output_dir/image_n4_mni_seg_post_volumes.csv`

**NOTE**:

In case post processing fails and the network output `*_seg.nii.gz` is preferred, check the last few lines of `bin/acapulco.sh` for applying the inverse transformation to bring the parcellation `*_parc.nii.gz` back to the original space.


[paper]: https://www.sciencedirect.com/science/article/pii/S1053811920303062
[docker]: https://gitlab.com/shuohan/keras-unet-cerebellum/container_registry
[sif_cpu]: http://iacl.jhu.edu/~shuo/data/acapulco_030.sif
[sif_gpu]: http://iacl.jhu.edu/~shuo/data/acapulco_030_gpu.sif
[ANTs]: http://stnava.github.io/ANTs/
